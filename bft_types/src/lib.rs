use std::path::Path;
use std::io;
use std::collections::HashMap;

/// Raw brainfuck instructions consisting of 8 possible commands
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum RawInstruction {
    Right,
    Left,
    Increment,
    Decrement,
    Output,
    Input,
    StartLoop,
    EndLoop,
}

use RawInstruction::*;

impl RawInstruction {
    /// Get a command from the corresponding character
    fn from_char(chr: char) -> Option<RawInstruction> {
        match chr {
            '>' => Some(Right),
            '<' => Some(Left),
            '+' => Some(Increment),
            '-' => Some(Decrement),
            '.' => Some(Output),
            ',' => Some(Input),
            '[' => Some(StartLoop),
            ']' => Some(EndLoop),
            _ => None
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Instruction {
    pub line: usize,
    pub col: usize,
    pub instr: RawInstruction,
}

#[derive(Debug)]
pub struct Program {
    filename: String,
    instr: Vec<Instruction>,
    brackets: HashMap<usize, usize>,
}

impl Program {
    pub fn new(filename: &AsRef<str>, input: &AsRef<str>) -> Program {
        let instr = input.as_ref()
            .lines()
            .enumerate()
            .flat_map(|(line, s)| {
                s.chars().enumerate()
                    .flat_map(move |(col, c)| RawInstruction::from_char(c)
                              .map(|instr| Instruction {line: line + 1, col: col + 1, instr}))
            })
            .collect();
        let brackets = HashMap::new();
        Program { filename: filename.as_ref().to_string(),
                  instr, brackets }
    }
    pub fn from_file(path: &AsRef<Path>) -> io::Result<Program> {
        let string : &AsRef<str> = &(std::fs::read_to_string(path)?);
        // If above worked then we have a valid string
        let filename = path.as_ref().to_str().unwrap();
        Ok(Program::new(&filename, string))
    }

    pub fn filename(&self) -> &String {
        &self.filename
    }

    pub fn instructions(&self) -> &Vec<Instruction> {
        &self.instr
    }

    /// Goes through program and checks that there is a matching number of
    /// opening and closing brackets
    /// It also changes the program so to place position of matching brackets
    /// in program structure
    pub fn check_brackets(&mut self) -> bool {
        use RawInstruction::*;
        let mut vec : Vec<(usize, usize, usize)> = vec![];
        for (pos, instr) in self.instructions().clone().iter().enumerate() {
            match instr {
                Instruction {line, col, instr: StartLoop} =>
                    vec.push((pos, *line, *col)),
                Instruction {line, col, instr: EndLoop} => {
                    if vec.is_empty() {
                        println!("Closing square bracket with no opening one at {} {}",
                                 line, col);
                        return false;
                    }
                    let (opening_pos, _, _) = vec.pop().unwrap();
                    self.brackets.insert(opening_pos, pos);
                    self.brackets.insert(pos, opening_pos);
                },
                _ => {}
            }
        }
        if !vec.is_empty() {
            println!("Opening square brackets with no closing one found at");
            for (_, line, col) in vec.iter() {
                println!("{}:{}", line, col);
            }
            false
        } else {
            true
        }
    }

    pub fn find_matching(&self, pc: usize) -> usize {
        *self.brackets.get(&pc).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_chars() {
        fn check(instr: RawInstruction, c: char) {
            assert_eq!(Some(instr), RawInstruction::from_char(c));
        }
        check(Right, '>');
        check(Left, '<');
        check(Increment, '+');
        check(Decrement, '-');
        check(Output, '.');
        check(Input, ',');
        check(StartLoop, '[');
        check(EndLoop, ']');

        // check that other characters give none
        fn check_none(c: char) {
            assert_eq!(None, RawInstruction::from_char(c));
        }
        check_none('#');
        check_none('a');
        check_none('p');
        check_none('9');
    }

    #[test]
    fn check_line_col() {
        let input = "    \n\n\n  -";
        let program = Program::new(&"test_input", &input);
        assert_eq!(*program.instructions(),
                   vec![Instruction { line: 4, col: 3, instr: Decrement }]);

        let input = "\n\n\n\n\n          asrocehusrcahoeu";
        let program = Program::new(&"test_input", &input);
        assert_eq!(*program.instructions(),
                   vec![]);
    }

    #[test]
    fn check_brackets_valid() {
        let input = "[][][][][[[]]][[][]]";
        let mut program = Program::new(&"test_input", &input);
        program.check_brackets();
    }

    #[test]
    fn check_brackets_invalid_openings() {
        let input = "[[[[[]]]";
        let mut program = Program::new(&"test_input", &input);
        program.check_brackets();
    }

    #[test]
    fn check_brackets_invalid_closings() {
        let input = "[[[]]]]";
        let mut program = Program::new(&"test_input", &input);
        program.check_brackets();
    }
}
