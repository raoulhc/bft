#[macro_use]
extern crate structopt;

use structopt::StructOpt;

use bft_types::Program;
use bft_interp::VM;

mod cli;

fn main() -> Result<(), Box<std::error::Error>> {
    let opt = cli::Opt::from_args();
    println!("{:?}", opt);
    let mut program = &mut Program::from_file(&opt.path.as_path())?;
    let n : usize = match opt.cells {
        Some(n) => n,
        None => 30_000
    };
    let mut vm : VM<u8> = VM::new(n, opt.extensible, &mut program);
    vm.load();
    match vm.interpret(&mut std::io::stdin(), &mut std::io::stdout()) {
        Ok(()) => Ok(()),
        Err(err) => Err(Box::new(err)),
    }
}
