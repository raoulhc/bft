use std::{io, fmt, error};

use bft_types::{Program, Instruction, RawInstruction};

#[derive(Debug)]
pub struct VM<'a, T> {
    array: Vec<T>,
    allow_grow: bool,
    head: usize,
    pc: usize,
    program: &'a mut Program
}

#[derive(Debug)]
pub enum VMError {
    InvalidHead(Instruction),
    IOError(io::Error, Instruction),
}

impl fmt::Display for VMError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for VMError {}

pub trait Wrapped {
    fn wrapped_add(&mut self);
    fn wrapped_sub(&mut self);
}

impl Wrapped for u8 {
    fn wrapped_add(&mut self) {
        if *self == std::u8::MAX {
            *self = 0;
        } else {
            *self += 1;
        }
    }
    fn wrapped_sub(&mut self) {
        if *self == 0 {
            *self = std::u8::MAX;
        } else {
            *self -= 1;
        }
    }
}

impl<'a, T> VM<'a, T>
where T: Default + Clone + Wrapped + From<u8> + Into<u8> + PartialEq + Eq {
    pub fn new(n: usize, grow: bool, program: &'a mut Program) -> VM<'a, T> {
        let arr = if n == 0 {
            vec![T::default(); 30_000]
        } else {
            vec![T::default(); n]
        };
        VM {
            array: arr,
            allow_grow: grow,
            head: 0,
            pc: 0,
            program }
    }

    pub fn load(&mut self) {
        self.program.check_brackets();
    }

    /// Move head to the right resulting in an error if it fell off the edge of
    /// the tape.
    pub fn move_head_right(&mut self) -> Result<usize, VMError> {
        if self.head == self.array.len() - 1 && !self.allow_grow {
            Err(VMError::InvalidHead(
                self.program.instructions()[self.pc]))
        } else {
            if self.allow_grow && self.head == self.array.len() - 1 {
                self.array.push(T::default())
            }
            self.head += 1;
            Ok(self.pc + 1)
        }
    }

    /// Move head to the left resulting in an error if it fell off the edge of
    /// the tape
    pub fn move_head_left(&mut self) -> Result<usize, VMError> {
        if self.head == 0 {
            Err(VMError::InvalidHead(
                self.program.instructions()[self.pc]))
        } else {
            self.head -= 1;
            Ok(self.pc + 1)
        }
    }

    pub fn increment_cell(&mut self) -> Result<usize, VMError> {
        self.array[self.head].wrapped_add();
        Ok(self.pc + 1)
    }

    pub fn decrement_cell(&mut self) -> Result<usize, VMError> {
        self.array[self.head].wrapped_sub();
        Ok(self.pc + 1)
    }

    pub fn read(&mut self, read : &mut io::Read) -> Result<usize, VMError> {
        let mut buf : [u8; 1] = [0];
        match read.read_exact(&mut buf[..]) {
            Ok(()) => Ok(()),
            Err(x) => Err(VMError::IOError(
                x, self.program.instructions()[self.pc])),
        }?;
        self.array[self.head] = T::from(buf[0]);
        Ok(self.pc + 1)
    }

    pub fn write(&mut self, write: &mut io::Write) -> Result<usize, VMError> {
        let x = self.array[self.head].clone();
        match write.write(&[T::into(x)]) {
            Ok(_) => Ok(self.pc + 1),
            Err(err) => Err(VMError::IOError(
                err, self.program.instructions()[self.pc]))
        }
    }

    pub fn start_loop(&mut self) -> Result<usize, VMError> {
        use RawInstruction::*;
        if self.array[self.head] == T::default() {
            let instr = self.program.instructions()[self.pc];
            match instr {
                Instruction {instr: StartLoop, ..} =>
                    Ok(self.program.find_matching(self.pc)),
                _ => panic!("Didn't load program")
            }
        } else {
            Ok(self.pc + 1)
        }
    }

    pub fn end_loop(&mut self) -> Result<usize, VMError> {
        use RawInstruction::*;
        if self.array[self.head] != T::default() {
            let instr = self.program.instructions()[self.pc];
            match instr {
                Instruction {instr: EndLoop, ..} =>
                    Ok(self.program.find_matching(self.pc)),
                _ => panic!("Samothing went wrong!")
            }
        } else {
            Ok(self.pc + 1)
        }
    }

    pub fn interpret<R, W>(&mut self, mut reader: &mut R, mut writer: &mut W)
                           -> Result<(), VMError>
    where
        R: io::Read,
        W: io::Write {
        use RawInstruction::*;
        loop { let next = match self.program.instructions()[self.pc] {
            Instruction {instr: x, ..} => match x {
                Right => self.move_head_right(),
                Left => self.move_head_left(),
                Increment => self.increment_cell(),
                Decrement => self.decrement_cell(),
                Output => self.write(&mut writer),
                Input => self.read(&mut reader),
                StartLoop => self.start_loop(),
                EndLoop => self.end_loop(),
            }};
               match next {
                   Ok(x) => if x >= self.program.instructions().len() {
                       return Ok(());
                   } else {
                       self.pc = x;
                   },
                   Err(err) => return Err(err),
               }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn wrapped_add() {
        let mut x : u8 = 0;
        x.wrapped_add();
        assert_eq!(x, 1);
        x.wrapped_add();
        x.wrapped_add();
        assert_eq!(x, 3);
    }

    #[test]
    fn wrapped_add_overflow() {
        let mut x : u8 = std::u8::MAX;
        x.wrapped_add();
        assert_eq!(x, 0)
    }

    #[test]
    fn wrapped_sub() {
        let mut x : u8 = 5;
        x.wrapped_sub();
        assert_eq!(x, 4);
        x.wrapped_sub();
        x.wrapped_sub();
        x.wrapped_sub();
        assert_eq!(x, 1);
    }

    #[test]
    fn wrapped_sub_overflow() {
        let mut x : u8 = 2;
        x.wrapped_sub();
        x.wrapped_sub();
        assert_eq!(x, 0);
        x.wrapped_sub();
        assert_eq!(x, std::u8::MAX);
    }

    #[test]
    fn increment_cell() {
        let mut program = Program::new(&"program", &"[]");
        let mut vm : VM<u8> = VM::new(0, false, &mut program);

        fn move_and_increment(vm : &mut VM<u8>) -> Result<usize, VMError> {
            vm.move_head_right()?;
            vm.move_head_right()?;
            vm.increment_cell()?;
            vm.increment_cell()?;
            vm.increment_cell()
        }

        let res : Result<usize, VMError> = move_and_increment(&mut vm);
        assert!(res.is_ok());
        assert_eq!(vm.array[2], 3)
    }

    #[test]
    fn decrement_cell() {
        fn move_and_decrement(vm : &mut VM<u8>) -> Result<usize, VMError> {
            vm.move_head_right()?;
            vm.move_head_right()?;
            vm.move_head_right()?;
            vm.move_head_right()?;
            vm.move_head_left()?;
            vm.decrement_cell()?;
            vm.decrement_cell()
        }

        let mut program = Program::new(&"program", &"[]");
        let mut vm : VM<u8> = VM::new(0, false, &mut program);

        let res = move_and_decrement(&mut vm);
        assert!(res.is_ok());
        let val = std::u8::MAX - 1;
        assert_eq!(vm.array[3], val);
    }

    #[test]
    fn test_read() {
        let mut program = Program::new(&"program", &"[]");
        let mut vm : VM<u8> = VM::new(0, false, &mut program);

        let mut cursor : io::Cursor<&[u8]> = io::Cursor::new(&[1, 2, 3]);

        fn read_and_move(vm: &mut VM<u8>, cursor: &mut io::Read)
                         -> Result<usize, VMError> {
            vm.read(cursor)?;
            vm.move_head_right()?;
            vm.read(cursor)?;
            vm.move_head_right()?;
            vm.read(cursor)
        }

        let res = read_and_move(&mut vm, &mut cursor);
        assert!(res.is_ok());
        assert_eq!(vm.array[0..3], [1, 2, 3])
    }

    #[test]
    fn test_write() {
        let mut program = Program::new(&"program", &"[]");
        let mut vm : VM<u8> = VM::new(0, false, &mut program);

        vm.array[0] = 1;
        vm.array[1] = 2;
        vm.array[2] = 3;

        let mut x = [0, 0, 0];
        let mut cursor : io::Cursor<&mut [u8]> = io::Cursor::new(&mut x[..]);

        fn write_and_move(vm: &mut VM<u8>,
                          cursor: &mut io::Write)
                          -> Result<usize, VMError> {
            vm.write(cursor)?;
            vm.move_head_right()?;
            vm.write(cursor)?;
            vm.move_head_right()?;
            vm.write(cursor)
        }

        let res = write_and_move(&mut vm, &mut cursor);
        assert!(res.is_ok());
        assert_eq!(x, [1, 2, 3]);
    }

    #[test]
    fn test_start_loop() {
        let mut program = Program::new(&"program", &"[---]");
        let mut vm: VM<u8> = VM::new(0, false, &mut program);
        vm.load();

        // check it jumps to close bracket when array head value == 0
        let res = vm.start_loop();
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), 4);

        // check it just goes to the next step when array head value != 0
        vm.pc = 0;
        vm.array[0] = 5;
        let res = vm.start_loop();
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), 1);
    }

    #[test]
    fn test_end_loop() {
        let mut program = Program::new(&"program", &"++[++<.,+]");
        let mut vm: VM<u8> = VM::new(0, false, &mut program);
        vm.load();

        // check it just goes one forward when array head == 0
        vm.pc = 9;
        let res = vm.end_loop();
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), 10);

        // check it jumps back when array head != 0
        vm.array[0] = 1;
        let res = vm.end_loop();
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), 2);
    }

    #[test]
    fn test_interpret() {
        let mut program = Program::new(&"program", &">>+++++");
        let mut vm: VM<u8> = VM::new(0, false, &mut program);
        vm.load();

        let mut x = [0,0,0];
        let mut y = [0,0,0];
        let mut reader: io::Cursor<&mut [u8]> = io::Cursor::new(&mut x[..]);
        let mut writer: io::Cursor<&mut [u8]> = io::Cursor::new(&mut y[..]);
        let res = vm.interpret(&mut reader, &mut writer);
        assert!(res.is_ok());
        assert_eq!(vm.array[2], 5);
    }

    #[test]
    fn test_interpret_fall_off() {
        let mut program = Program::new(&"program", &">><<<++");
        let mut vm: VM<u8> = VM::new(0, false, &mut program);
        vm.load();

        let mut x = [0,0,0];
        let mut y = [0,0,0];
        let mut reader: io::Cursor<&mut [u8]> = io::Cursor::new(&mut x[..]);
        let mut writer: io::Cursor<&mut [u8]> = io::Cursor::new(&mut y[..]);
        let res = vm.interpret(&mut reader, &mut writer);
        assert!(res.is_err());
        assert_eq!(vm.head, 0);
        assert_eq!(vm.pc, 4);
    }
}
