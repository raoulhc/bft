use std::path::PathBuf;

#[derive(Debug, StructOpt)]
#[structopt(name = "BF Interpreter",
            about = "Command line interpreter for brain fuck written in rust")]
pub struct Opt {
    /// Input file path
    #[structopt(name = "PROGRAM", parse(from_os_str))]
    pub path: PathBuf,
    /// Number of cells in array
    #[structopt(long = "cells", short = "c")]
    pub cells: Option<usize>,
    /// Whether the array is allowed to grow
    #[structopt(short = "e")]
    pub extensible: bool,
}
